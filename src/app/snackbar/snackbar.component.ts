import { Component } from '@angular/core';
import { MdSnackBar } from '@angular/material';

@Component({
  selector: 'app-snackbar',
  templateUrl: './snackbar.component.html',
})
export class SnackbarComponent {

  constructor(public snackBar: MdSnackBar) {

    this.snackBar.open('Falha no login verifique o email e senha', 'teste',{
      duration: 3000,
    });
  }

  teste() {
    console.log('Aooow cambelo');
    
  }

  // openSnackBar(message: string, action: string) {
  //   this.snackBar.open(message, action, {
  //     duration: 2000,
  //   });

}
